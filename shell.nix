let
  # nixpkgs-24.11-darwin
  # 2025-02-26
  nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/archive/2f137a92a87d2e03e75fce3c9c9073095720369b.tar.gz";
  pkgs = import nixpkgs { config = {}; overlays = []; };
in
pkgs.mkShell {
  packages = with pkgs; [
    emacs
    git
    nodejs_23
  ];
}
