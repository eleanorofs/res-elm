*This package is a fork of 
[bs-elm-es6](https://www.npmjs.com/package/bs-elm-es6), which is still being 
actively maintained. However, this project uses the
latest ReScript syntax. If you are familiar with the new syntax, you will
probably be most comfortable with this project.*

# res-elm
## a standalone package for interoperation between Elm and ReScript.

## Philosophy

This is a standalone binding for Elm 0.19 ports. It has no dependencies and 
can be used without a bundler. It is meant to be highly idiomatic to 
JavaScript developers. 

## Installation
`npm install res-elm` in your console and then add `"res-elm"` to your
bs-dependencies. 

## Usage

1. *(Optional)* Expose the module in your Index.res, like so:
```ocaml
open Elm;
```
    
2. Define your `ports` type as a record, where each field is either a 
`sendable` of a type or a `subscribable` of a type. For example: 

```ocaml
type ports = 
{
    toElm: Elm.sendable<string>,
    fromElm: Elm.subscribable<string>
};
```
2. Get your target node and initialize your app. For example: 
```ocaml
    @val @scope ("document")
    external getElementById: string => Dom.element = "getElementById";
    let app: Elm.app<ports> = 
      Elm.Main.initWithOptions({ node: Some(getElementById("elm-target")),
                                 flags: None
                               });
```
3. Use your ports. There are two ways to do this. 

### The ReScript-y Way to use ports. 
`res-elm` exposes methods `Elm.send('model, sendable<'model>)` and 
`Elm.subscribe(('model => unit), subscribable<'model>)`. 

You can pipe into these methods like so:
```ocaml
app.ports.toElm -> Elm.send("Hello, from ReScript.");
app.ports.fromElm -> Elm.subscribe(str => Js.log(str));
```
I imagine this is the primary way most users will interact with this package.

### The JavaScript-y Way to use ports.

`res-elm` also exposes a less-idiomatic-to-ReScript way to interact with
Elm ports. That is, `sendable<'model>` and `subscribable<'model>` are both 
records with a function-valued field. 

That is, you can literally call the object method directly in ReScript like: 
```ocaml
app.ports.toElm.send("Hello, from ReScript");
```
    
This is kind of a cringe-y hack and I don't imagine it will be widely used, 
but it may be useful if you're coming from the JavaScript/Elm world or if 
you want a faster refactor from JavaScript to ReScript. (Plus, I really just 
wanted to see if something like this would work, and it does.)

## Example
There's a fully functioning, minimal example at 
https://elmandrescript.gitlab.io which includes one incoming and one 
outgoing port. It is explained further in 
[the article I wrote about it](https://webbureaucrat.gitlab.io/posts/writing-elm-ports-in-rescript/).

Let me know if you have any questions.

