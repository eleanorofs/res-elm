type app<'ports> = { ports: 'ports };

type initOptions<'flags> = {
  node: option<Dom.element>,
  flags: option<'flags>
};

module Main = {
  @scope(("Elm", "Main")) @val
  external init: () => app<'ports> = "init";

  @scope(("Elm", "Main")) @val
  external initWithOptions: initOptions<'flags> => app<'ports> = "init";
}

type elmHandler<'model> = 'model => unit

type sendable<'model> = {send: elmHandler<'model>}

type subscribable<'model> = {subscribe: ('model => unit) => unit}

@send external send: (sendable<'model>, 'model) => unit = "send"
@send external subscribe: (subscribable<'model>, 'model => unit) => unit
  = "subscribe"
